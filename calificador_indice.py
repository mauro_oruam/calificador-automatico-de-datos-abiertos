import zipfile
import io
import json
import requests
import fuzzywuzzy

import pandas as pd

from dateutil.parser import parse
from collections import Counter
from fuzzywuzzy import process


def calificador(dframe, estandar_elegido, umbral=85):
    """
    Compara los nombres de las columnas del archivo elegido con el estandar correspondiente a su categoria
    y realiza un informe de las coincidencias y los errores entre ambos.


    Parameters
    ----------
    archivo: str
      el nombre del archivo a comparar

    estandar_elegido:str
      el estandar con el que se comparará el archivo

    sep_decimal: El cacarter usado para separar decimales, puede ser ',' o '.'

    Return
    ----------
    resultados: dict
      un diccionario con 5 elementos: Una lista con las columnas aprobadas, un diccionario con las
      columnas casi aprobadas, que contiene el nombre incorrecto utilizado en el archivo, el nombre
      correcto del estandar al que hace referencia y el porcentaje de error entre ambos), una lista
      con las columnas fuera de estandar, una lista con las columnas faltantes y una lista con las
      estadísticas generales de la comparación realizada, cuyo primer valor es la cantidad de
      elementos y el segundo el porcentaje que representan estas del total.
    """
    # cargamos json de estandares
    with open('./taxonomias_de_estandares.csv', 'r') as f:
        estandares = pd.read_csv('./taxonomias_de_estandares.csv')

    # creamos lista de estándares válidos
    lista_estandares = estandares['sub_categoria'].unique()

    # verificamos si el estandar elegido existe en la lista de estándares válidos
    if estandar_elegido not in lista_estandares:
        print(estandar_elegido)
        return {'error': 'No se encuentra el estandar'}

    # inicializamos listas
    lista_faltantes = []
    lista_100 = []
    lista_85 = []
    lista_0 = []
    columnas_casi_aprobadas = []
    casi_aprobadas = []
    columnas_valor_unico = []
    df = dframe
    columnas = df.columns.tolist()
    estandar = estandares.loc[estandares['sub_categoria'] == estandar_elegido, 'col_name'].tolist()
    estandar_len = len(estandar)

    # chequeamos columnas sin información
    for col in columnas:
        if len(df[col].unique()) == 1:
            columnas_valor_unico.append(col)

    # Creamos las listas con fuzzy match
    for col in columnas:
        r = process.extractOne(col, estandar, scorer=fuzzywuzzy.fuzz.ratio)

        #lista 100
        if r and r[1] == 100:


            dtype = estandares.loc[estandares['col_name'] == r[0], 'dtype'].values[0]

            if dtype == 'date':
                try:
                    pd.to_datetime(df[col])
                    lista_100.append({'col_name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                except:
                    lista_100.append({'col_name':r[0], 'correct dtype':False,
                                     'expected dtype':'date'})
                    estandar.remove(r[0])
            elif dtype == 'numeric':
                if df[col].dtype == float or df[col].dtype == int:
                    lista_100.append({'col_name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                else:
                    lista_100.append({'col_name':r[0], 'correct dtype':False,
                                     'expected dtype':'numeric'})
                    estandar.remove(r[0])
            else:
                lista_100.append({'col_name':r[0], 'correct dtype':True})
                estandar.remove(r[0])

        #lista 85
        elif r and umbral <= r[1] < 100:
            dtype = estandares.loc[estandares['col_name'] == r[0], 'dtype'].values[0]
            if dtype == 'date':
                try:
                    pd.to_datetime(df[col])
                    lista_85.append({'col_name':col,'expected name':r[0],'correct dtype':True})
                    estandar.remove(r[0])
                except:
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':False,
                                    'expected dtype':'date'})
                    estandar.remove(r[0])
            elif dtype == 'numeric':
                if df[col].dtype == float or df[col].dtype == int:
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                else:
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':False,
                                     'expected dtype':'numeric'})
                    estandar.remove(r[0])
            else:
                lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':True})
                estandar.remove(r[0])

        #lista 0
        else:
            lista_0.append(col)

    # creamos el diccionario de las casi aprobadas
    columnas_casi_aprobadas = lista_85
    dtypes_correctos = sum([1 for l in lista_100 if l['correct dtype']]) + sum([1 for l in lista_85 if l['correct dtype']])

    # creamos el diccionario con el informe
    resultados = {
        'columnas aprobadas': lista_100,
        'columnas casi aprobadas': columnas_casi_aprobadas,
        'columnas fuera de estandar': lista_0,
        'faltantes del estandar': estandar,
        'estadisticas': {
            'columnas aprobadas': {'absoluto': len(lista_100),
                                   'porcentaje': round(len(lista_100)*100 / len(columnas), 2)},
            'columnas casi aprobadas': {'absoluto':len(columnas_casi_aprobadas),
                                        'porcentaje':round(len(columnas_casi_aprobadas)*100 / len(columnas), 2)},
            'columnas fuera de estandar': {'absoluto':len(lista_0)},
            'faltantes del estandar': {'absoluto': len(estandar),
                                       'porcentaje': round(len(estandar)*100/estandar_len, 2)},
            'columnas con valor único': {'absoluto':columnas_valor_unico,
                                         'porcentaje':round(len(columnas_valor_unico) * 100 / len(columnas), 2)},
            'dtypes correctos': dtypes_correctos
        }
    }




    return resultados
