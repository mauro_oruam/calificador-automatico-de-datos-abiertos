# Calificador de Datos Abiertos gubernamentales

Este proyecto ofrece una herramienta para analizar las tablas en formatos CSV, XLS y XLSX que publican, en Internet, los gobiernos municipales, provinciales y nacionales, contrastándolas con los estándares correspondientes para su categoría. Los estándares definen las columnas que debe tener cada tabla y el tipo y formato de dato que debe almacenar cada una. 
Para analizar múltiples archivos se debe crear un libro de hojas de cálculo en el que cada hoja tenga el nombre del estandar relevado, y dentro de cada una las columnas: "jurisdiccion" (con el nombre de la jursdicción relevada en cada fila), "¿donde encontraste los datos?" (en la que se listen los URL desde donde se descarga cada archivo) y "format type" (en la que se estipule el tipo de archivo que se descarga desde el link). El programa solo intentará analizar aquellos links que figuren con tipo de formato "CSV", "XLS" o "XLSX" (respetar mayúsculas). Los estándares válidos están listados al final. 
Si el archivo que se descarga de algún link está dentro de un fichero comprimido, el programa buscará el archivo CSV, XLS o XLSX  dentro de los directorios hasta encontrarlo, de no existir seguirá con el siguiente URL. 
La aplicación creará, si no la encuentra en el directrio raíz, la carpeta "Descargas_calificador " y comenzará a descargar allí todos los archivos y a analizarlos. Al finalizar, devolverá, dentro de un Datapackage, tres archivos:
> resultados.csv:  Se compone del puntaje de cada jurisdicción con respecto a cada estandar relevado.

> resultados_completo.json: Se compone de toda la información del análisis, incluyendo las columnas presentes, faltantes y sobrantes de cada archivo en relación al estandar.

> error_log.json: Con los errores que surgieron durante la descarga y proceso de archivos.

## Instalación:
$ git clone indice-de-datos
$ cd nombre de repo 
$ pip install requirements.txt

Las columnas y tipos de datos de cada estandar válido se encuentran en el archivo "taxonomias_de_estandares.csv", se pueden agregar más estándares respetando el formato del archivo.

### Estándares válidos:
Gasto público
Compras y contrataciones
Pauta oficial
Estructura orgánica
Sueldo
DDJJ
Límites administrativos
Lugares públicos
Catastro
Terrenos fiscales
Transporte público
Paradas de transporte público
Recorridos de transporte público
Horarios de transporte público
Resultados electorales
Actividad legislativa
Obras públicas
Medio ambiente
Calidad de aire
Calidad de agua
Ordenanzas
Centros de atención a víctimas de violencia de género
Pedidos de acceso a información pública
Presupuesto gobierno


### ¿Como se usa?


Desde la terminal ingresar "python", el nombre del archivo calificador (por default calificador_para_terminal.py), seguido de -f o --file y nombre del archivo a analizar, sin comillas, luego -d para el año del análisis y -r para la región.

```$ python calificador_para_terminal.py -f *archivo_a_analizar*.xlsx -d 2018 -r Argentina
```


### Tipos de error

no se encuentra el estandar: El nombre del estandar solicitado no se encuentra entre los estándares predeterminados y deberá agregarse a la lista.
error al descargar el archivo: El archivo no se ha podido descargar por URL malformada o porque el servidor no responde.
no se pudo procesar el archivo: El archivo tiene un formato no válido.

### ¿Como colaborar?

Se puede colaborar agregando estandares todavía no definidos y agregando funcionalidades extra para la devolución de la evaluación realizada. 



