from calificador_indice import calificador
from flask_restful import Resource, reqparse
from dateutil.parser import parse
import patoolib
import os
import requests
import pandas as pd
parser = reqparse.RequestParser()
parser.add_argument('dataset_url', required=True, action='append', help='Rate cannot be converted')
parser.add_argument('taxonomia', required=True, help='foo fa bar')
parser.add_argument('sep_decimal', required=True, help='foo fa bar')

class Calificador(Resource):
    def get(self):
        return {'hello': 'world'}

    def post(self):
        args = parser.parse_args()
        print(args)


        if not os.path.isdir('descargas'):
            os.makedirs('descargas')
        #definimos nombre de archivo
        for url in args.dataset_url:
            if url.split('/')[-1] != '':
                nombre_archivo = url.split('/')[-1]
            else:
                nombre_archivo = url.split('/')[-2]
        for decimal in args.sep_decimal:
            sep_decimal = decimal
            #comienza a descargar el archivo
            try:
                descarga = requests.get(url, stream=True)
                with open('descargas/' + nombre_archivo, 'wb') as f:
                    for chunk in descarga.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)
            except(requests.exceptions.ConnectionError):
                return({'error': 'no se pudo descargar '+ nombre_archivo})

                #Chequea que la descarga sea un archivo
            if os.path.isfile('descargas/'+nombre_archivo):
                #si es un archivo comprimido extraerá el arhivo que se encuentra dentro
                if nombre_archivo.endswith('.rar') or nombre_archivo.endswith('.tar') or nombre_archivo.endswith('.zip') :
                    pl = patoolib.extract_archive(nombre_archivo, outdir='/descargas')
                    #si es un archivo válido lo convertirá a Dataframe
                    if os.path.isfile('descargas/'+nombre_archivo):
                        if pl.endswith('.xls') or pl.endswith('.xlsx'):
                            try:
                                df = pd.read_excel('descargas/' + pl, sep=sep_decimal)
                            except:
                                df = pd.read_excel('descargas/' + pl, encoding='latin-1',sep=sep_decimal)

                    elif pl.endswith('.csv'):
                        try:
                            df = pd.read_csv('descargas/' + pl,sep=sep_decimal)
                        except:
                            df = pd.read_csv('descargas/' + pl, sep=sep_decimal, encoding='latin-1')
                    else:
                        print({'error': 'no se pudo procesar el archivo'})

                #si es un archivo csv o read_excel
                elif nombre_archivo.endswith('.xls') or nombre_archivo.endswith('.xlsx'):
                    df = pd.read_excel('descargas/' + nombre_archivo, sep=sep_decimal)

                #Problema aqui
                elif nombre_archivo.endswith('.csv'):
                    try:
                        df = pd.read_csv('descargas/' + nombre_archivo, sep=sep_decimal)
                    except:
                        try:
                            df = pd.read_csv('descargas/' + nombre_archivo, sep=sep_decimal,  encoding='latin-1')
                        except:
                            print({'error': 'no se pudo procesar el archivo'})
                            pass
                else:
                    pass
            #si la descarga es un directorio, buscará dentro de este el documento CSV, XLS o XLSX
            elif os.path.isdir('descargas/'+nombre_archivo):
                carpetas = os.listdir('descargas/'+nombre_archivo)
                for i in carpetas:
                    for item in os.listdir('descargas/'+nombre_archivo+'/'+i):
                            if item.endswith('.xls') or item.endswith('.xlsx'):
                                try:
                                    df = pd.read_excel('descargas/' +nombre_archivo + item,
                                                       sep=sep_decimal)
                                except:
                                    df = pd.read_excel('descargas/' +nombre_archivo + item,
                                                       encoding='latin-1', sep=sep_decimal)

                            elif item.endswith('.csv'):
                                try:
                                    df = pd.read_csv('descargas/' +nombre_archivo + item, sep=sep_decimal)
                                except:
                                    df = pd.read_csv('descargas/' +nombre_archivo + item, sep=sep_decimal,
                                                     encoding='latin-1')
                            else:
                                {'error': 'no se pudo procesar el archivo'}
                                continue
            else:
                print({'error': 'no se pudo procesar el archivo'})
                break

            r = calificador(df, args['taxonomia'])

            return r
