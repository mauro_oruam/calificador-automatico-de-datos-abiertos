from flask import Flask
from flask_restful import Api

from views.home import Home
from views.test import Test
from views.evaluador import Calificador

app = Flask(__name__)
api = Api(app)

#
api.add_resource(Home, '/')
api.add_resource(Test, '/test')
api.add_resource(Calificador, '/evaluador')

if __name__ == '__main__':
    app.run(debug=True)
