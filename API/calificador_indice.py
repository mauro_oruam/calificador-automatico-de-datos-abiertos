
# coding: utf-8

# In[1]:


import zipfile
import io
import json
from dateutil.parser import parse

import requests
import fuzzywuzzy
import pandas as pd
from fuzzywuzzy import process


# In[5]:


def calificador(dframe, estandar_elegido, umbral=85):
    """
    Compara los nombres de las columnas del archivo elegido con el estandar correspondiente a su categoria
    y realiza un informe de las coincidencias y los errores entre ambos.

    
    Parameters
    ----------
    archivo: str
      el nombre del archivo a comparar
      
    estandar_elegido:str
      el estandar con el que se comparará el archivo
    
    sep_decimal: El cacarter usado para separar decimales, puede ser ',' o '.'
      
    Return
    ----------
    resultados: dict
      un diccionario con 5 elementos: Una lista con las columnas aprobadas, un diccionario con las 
      columnas casi aprobadas, que contiene el nombre incorrecto utilizado en el archivo, el nombre 
      correcto del estandar al que hace referencia y el porcentaje de error entre ambos), una lista 
      con las columnas fuera de estandar, una lista con las columnas faltantes y una lista con las
      estadísticas generales de la comparación realizada, cuyo primer valor es la cantidad de 
      elementos y el segundo el porcentaje que representan estas del total.
    """
    # cargamos json de estandares
    with open('estandares.json', 'r') as f:
        estandares = json.load(f)
        
    if estandar_elegido not in estandares:
        return {'error': 'No se encuentra el estandar'}
        
    # inicializamos listas
    lista_faltantes = []
    lista_100 = []
    lista_85 = []
    lista_0 = []
    columnas_casi_aprobadas = []
    casi_aprobadas = []
    columnas_valor_unico = []
    df = dframe
    columnas = df.columns.tolist()
    estandar = [c['col_name'] for c in estandares[estandar_elegido]].copy()
    estandar_len = len(estandar)
        
    # chequeamos columnas sin información
    for col in columnas:
        if len(df[col].unique()) == 1:
            columnas_valor_unico.append(col)
            
    # Creamos las listas con fuzzy match    
    for col in columnas:
        r = process.extractOne(col, estandar, scorer=fuzzywuzzy.fuzz.ratio)
        
        #lista 100
        if r and r[1] == 100:
            print(col)
            
            dtype = [c for c in estandares[estandar_elegido] if c['col_name'].lower() == col.lower()][0]['dtype']
            if dtype == 'date':
                try:
                    pd.to_datetime(df[col])
                    lista_100.append({'col_name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                except:
                    lista_100.append({'col_name':r[0], 'correct dtype':False,
                                     'expected dtype':'date'})
                    estandar.remove(r[0]) 
            elif dtype == 'numeric':
                if df[col].dtype == float or df[col].dtype == int: 
                    lista_100.append({'col_name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                else:
                    lista_100.append({'col_name':r[0], 'correct dtype':False,
                                     'expected dtype':'numeric'})
                    estandar.remove(r[0])
            else:
                lista_100.append({'col_name':r[0], 'correct dtype':True})
                estandar.remove(r[0])
             
        #lista 85
        elif r and umbral <= r[1] < 100:
            dtype = [c for c in estandares[estandar_elegido] if c['col_name'] == r[0]][0]['dtype']
            if dtype == 'date':
                try:
                    pd.to_datetime(df[col])
                    lista_85.append({'col_name':col,'expected name':r[0],'correct dtype':True})
                    estandar.remove(r[0])
                except:
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':False,
                                    'expected dtype':'date'})
                    estandar.remove(r[0]) 
            elif dtype == 'numeric':
                if df[col].dtype == float or df[col].dtype == int: 
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':True})
                    estandar.remove(r[0])
                else:
                    lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':False,
                                     'expected dtype':'numeric'})
                    estandar.remove(r[0])
            else:
                lista_85.append({'col_name':col,'expected name':r[0], 'correct dtype':True})
                estandar.remove(r[0])
            
        #lista 0
        else:
            lista_0.append(col)     
            
    # creamos el diccionario de las casi aprobadas
    columnas_casi_aprobadas = lista_85
    
    # creamos el diccionario con el informe
    resultados = {
        'columnas aprobadas': lista_100,
        'columnas casi aprobadas': columnas_casi_aprobadas,
        'columnas fuera de estandar': lista_0,
        'faltantes del estandar': estandar,
        'estadisticas': {
            'columnas aprobadas': [len(lista_100), round(len(lista_100)*100 / 
                                   len(columnas), 2)],
            'columnas casi aprobadas': [len(columnas_casi_aprobadas),round(len(columnas_casi_aprobadas)*100 / 
                                       len(columnas), 2)], 
            'columnas fuera de estandar': [len(lista_0),round(len(lista_0)*100 /
                                           len(columnas), 2)],
            'faltantes del estandar': [len(estandar),round(len(estandar)*100/estandar_len, 2)],
            'columnas con valor único': [columnas_valor_unico, round(len(columnas_valor_unico)*100/
                                        len(columnas), 2)]
        }
    }
      
        
                
    
    return resultados

