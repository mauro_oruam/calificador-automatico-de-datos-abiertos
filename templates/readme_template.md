#EVALUACIÓN AUTOMÁTICA DE DATOS ABIERTOS GUBERNAMENTALES

>Este paquete contiene una evaluación de calidad de datos abiertos gubernamentales realizada de manera automática con el Calificador de Estándares de la fundación Conocimiento Abierto. 
>Se han comparado las columnas presentes en cada dataset, y su tipo de dato correspondiente, con aquellas que deberían tener para cumplir con el estandar de su categoría. 

##Contenido:

-Datapackage.json: Este archivo contiene toda la metadata del paquete, del análisis realizado y del esquema de archivos que lo compone.

-data: Es el directorio que contiene los archivos con los resultados del análisis en formatos CSV y JSON. 

-scripts: Este directorio contiene los scripts de python utilizados para realizar en análisis a partir del dataset suministrado por el usuario.


###Esquema de resultados:

#resultados_completo.json:

jurisdiccion: La jurisdicción a la que pertenece el documento gubernamental analizado.
nombre_archivo: Nombre del archivo descargado desde el sitio de datos abiertos.
estandar: El estándar al que corresponde el documento analizado. 
resultados: Objeto con resultados del análisis.
columnas aprobadas: Lista con las columnas del documento analizado idénticas a las definidas por el estándar.
columnas casi aprobadas: Lista con columnas del documento analizado que solo difieren en uno o dos caracteres de las definidas por el estándar. 
columnas fuera de estandar: Lista de columnas que se encuentran en el documento pero no en el estándar.
faltantes del estandar: Lista con columnas del estándar faltantes en el documento analizado. 
estadisticas: Objeto con las estadísticas del análisis resumidas.	
columnas aprobadas:	Objeto con el número de columnas aprobadas y porcentaje que representan del total.
columnas casi aprobadas: Objeto con el número de columnas casi aprobadas, con una tolerancia de uno a dos caracteres, y porcentaje que representan del total.
columnas fuera de estandar: Columnas que se encuentran en el documento analizado pero no en el estándar correspondiente.
faltantes del estandar: Columnas del estándar correspondiente que no se encuentran en el documento analizado.
columnas con valor único: Objeto con el número de columnas del documento analizado que tienen un valor único repetido en todas las filas y el porcentaje que representan del total.	
dtypes correctos: Cantidad de archivos cuyo tipo se corresponde con el definido por el estándar. 


#error_log.json:

archivo: Nombre del archivo analizado.
url: La url desde la que se descargó el archivo analizado.
error: Tipo de error con el que se encontró el calificador al intentar analizar el archivo.


#resultados.csv:

jurisdiccion: La jurisdicción a la que pertenece el documento gubernamental analizado.
estandar: El estándar al que corresponde el documento analizado. 
columnas_aprobadas: Cantidad de columnas correspondientes al estándar en el dataset.
porcentaje_columnas_aprobadas: Porcentaje que representan las columnas aprobadas en relación al total de columnas del dataset.
columnas_casi_aprobadas: Cantidad de columnas que difieren en no más de dos caracteres de las columnas estipuladas por el estándar correspondiente.
procentaje_columnas_casi_aprobadas: Porcentaje que representan las columnas casi aprobadas del total de columnas del dataset.
columnas_faltantes: Cantidad de columnas que faltan en el dataset en relación al estándar correspondiente.
porcentaje_columnas_faltantes: Porcentaje que representan las columnas faltantes en relación al total de columnas estipuladas en el estándar correspondiente.
columnas_fuera_de_estandar: Cantidad de columnas presentes en el dataset no contempladas en el estándar correspondiente.
porcentaje_columnas_fuera_de_estandar: Porcentaje que representan las columnas fuera de estandar en relación al total de columnas del dataset.
dtypes_correctos: Cantidad de columnas con tipo de dato correcto en el dataset.




