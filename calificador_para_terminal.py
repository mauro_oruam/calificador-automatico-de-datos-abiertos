#imports
import os
import sys
import json
import time
import patoolib
import requests
import argparse
import magic
import shutil
import pandas as pd
from shutil import copyfile
from chardet.universaldetector import UniversalDetector
from calificador_indice import calificador

#iniciamos el parser para recibir argumentos desde la terminal
parser = argparse.ArgumentParser(description='Procesa un libro de hojas de cálculo.')
parser.add_argument('-f','--file', type=str, help='indicar el archivo de hojas múltiples para el análisis de estandar')
parser.add_argument('-r','--region', type=str, help='indicar el nombre de la región a analizar')
parser.add_argument('-d','--date', type=str, help='indicar el período que representan los datos a analizar')
args = parser.parse_args()
archivo = args.file
lugar = args.region
periodo = args.date


#lista de errores
errores=[]

#cargamos los estandares desde el archivo json
df = pd.read_csv('./taxonomias_de_estandares.csv')
estandares = df['sub_categoria'].unique()


#Definimos las variables de tiempo para nombrar los archivos
timestr = time.strftime("%d"+"-"+"%m"+"-"+"%Y")
year = time.strftime("%Y")

#Creamos la carpeta para los resultados, con la fecha del día en que se ejecuta el análisis, y la carpeta scripts.
if os.path.isdir('./calidad-datos-abiertos-{0}/'.format(timestr)):
    shutil.rmtree('./calidad-datos-abiertos-{0}/'.format(timestr))
    os.makedirs('./calidad-datos-abiertos-{0}/data/'.format(timestr))
    os.makedirs('./calidad-datos-abiertos-{0}/scripts/'.format(timestr))
else:
    os.makedirs('./calidad-datos-abiertos-{0}/data/'.format(timestr))
    os.makedirs('./calidad-datos-abiertos-{0}/scripts/'.format(timestr))

#Copiamos los scripts a su nueva carpeta.
if os.path.isdir('./Indice-de-datos/Calificador-automatico-de-datos-abiertos'):
    copyfile('./Indice-de-datos/Calificador-automatico-de-datos-abiertos/calificador_indice.py', './calidad-datos-abiertos-{0}/scripts/calificador_indice.py'.format(timestr))
    copyfile('./Indice-de-datos/Calificador-automatico-de-datos-abiertos/calificador_para_terminal.py', './calidad-datos-abiertos-{0}/scripts/calificador_para_terminal.py'.format(timestr))



#Abre el template del Datapackage.
with open("./templates/datapackage_template.json", "r") as jsonFile:
    data = json.load(jsonFile)

#Completamos la descripción del Datapackage
data['description'] = 'Resultados de la evaluación de calidad de datos abiertos gubernamentales de {0}, \
                       durante el período {1}, según estándares internacionales recopilados por la\
                       Fundación Conocimiento Abierto, desglozados en CSV y JSON estandarizados y \
                       normalizados.'.format(lugar,periodo)

#Completamos el campo 'last updated'
data['last_updated'] = timestr

#Completamos la versión del paquete
data['version'] = year

#Completamos el nombre del archivo con los resultados completos
data['resources'][0]['name'] = "resultados_completos_{0}_{1}".format(lugar,timestr)

#Completamos el nombre del log de errores
data['resources'][2]['name'] = "error_log_{0}_{1}".format(lugar,timestr)

#
with open("./calidad-datos-abiertos-{0}/datapackage.json".format(timestr), "w") as jsonFile:
    json.dump(data, jsonFile)



#función que inicia el análisis
def calificador_terminal():
    """
    inicia el análisis a partir del archivo ingresado por terminal y guarda los
    resultados en la variable 'análisis_completo', para luego convertirlos en los
    tres archivos json que devuelve : resultados_completo y
    error_log.

    """

    #remueve las carpetas de descargas y de resultados del calificador si ya existen en el directorio
    if os.path.isdir('./descargas_calificador'):
        shutil.rmtree('./descargas_calificador')


    #realiza el análisis y devuelve las listas resultados y lista_dict.
    analisis_completo = {
                         'resultados':[],
                         'lista_dict':[],
                         'error_log':[]
                        }
    path_index = archivo
    xl = pd.ExcelFile(path_index)
    sheet_name = xl.sheet_names
    for i, j in enumerate(sheet_name):
        df = pd.read_excel(archivo, sheet_name=i)
        df['tipo de formato'] = df['tipo de formato'].fillna('')
        df['tipo de formato'] = df['tipo de formato'].str.upper()
        print(sheet_name[i])

        sheet_results=correr_analisis(df, sheet_name[i])
        if sheet_results != 'empty':
            analisis_completo['resultados'] = analisis_completo['resultados'] + sheet_results['resultados']
            analisis_completo['lista_dict'] = analisis_completo['lista_dict'] + sheet_results['lista_dict']
        else:
            continue

    #Creamos el dataframe con los resultados
    df = pd.DataFrame(analisis_completo['lista_dict'])
    df = df[['jurisdiccion', 'estandar', 'columnas_aprobadas', 'porcentaje_columnas_aprobadas','columnas_casi_aprobadas',
             'porcentaje_columnas_casi_aprobadas', 'columnas_faltantes','porcentaje_columnas_faltantes',
             'columnas_fuera_de_estandar', 'porcentaje_columnas_fuera_de_estandar','dtypes_correctos','url']]

    #Creamos el CSV a partir del dataframe
    df.to_csv('./calidad-datos-abiertos-{0}/data/resultados.csv'.format(timestr), index=False)

    #creamos los archivos json de salida a partir de los resultados
    with open('./calidad-datos-abiertos-{0}/data/resultados_completo_{0}_{1}.json'.format(timestr, lugar), 'w') as resultados_completo:
        resultados_completo.write(json.dumps([analisis for analisis in analisis_completo['resultados']]))
    with open('./calidad-datos-abiertos-{0}/data/error_log_{0}_{1}.json'.format(timestr, lugar), 'w') as error_log:
        error_log.write(json.dumps([analisis for analisis in errores]))





    print ('análisis finalizado, se crearon los archivos: resultados.csv, resultados_completo.json y error_log.json')


#función para detectar encoding
def detect_encoding(file_path):
    """
    Detecta el encoding de un archivo csv, xls o xlsx, devuelve un string con
    el nombre del encoding detectado

    Parameters:
    file_path: es la dirección en el sistema del archivo a analizar.

    """
    detector = UniversalDetector()
    detector.reset()
    with open('./descargas_calificador/'+file_path, mode='rb') as f:
        for b in f:
            detector.feed(b)
            if detector.done: break
    detector.close()
    return detector.result['encoding']


#función que descarga los archivos desde las URL del archivo elegido
def bajar_archivo(url, nombre_archivo, jurisdiccion):
    """
    Descarga los archivos a partir de la url pasada como argumento y les asigna un nombre
    indicado en el segundo argumento. La función irá indicando el porcentaje de descarga y
    en caso de error informará en consola.

    Parameters:
    url: Es la dirección desde la que debe descargar el archivo
    nombre_archivo: Es el nombre con el que se identificará al archivo descargado.
    jurisdiccion: es el nombre de la jurisdiccion del archivo descargado que
    aparecerá en la terminal mientras se descarga.
    """

    #si no existe la carpeta descargas_calificador en el directorio raíz, la crea.
    if not os.path.isdir('./descargas_calificador'):
        os.makedirs('./descargas_calificador')

    #comienza a descargar el archivo
    print('Comenzando descarga de archivo')
    cnt = 0
    try:
        r = requests.get(url, stream=True)

        if r.status_code < 303:
            if 'Content-Length' in r.headers:
                size = int(r.headers['Content-Length'])
            elif 'Content-Range' in r.headers:
                    url.split('/')[-1]
                    size = int(r.headers['Content-Range'].split('/')[-1])

            else:
                size = 0
            with open('./descargas_calificador/' + nombre_archivo, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):

                    cnt += 1024
                    if size:
                        print('\r'+ jurisdiccion.upper() +':'+nombre_archivo[0:30]+'-bajando {0}%'.format(round(cnt*100/size),2), end='')
                    else:
                        print('\r'+ jurisdiccion.upper() + ':'+nombre_archivo[0:30], end='')
                    if chunk:
                        f.write(chunk)
                print('\r'+ jurisdiccion.upper()+':'+ nombre_archivo[0:30]+'-bajando {0}%'.format(100.00,))
        else:
            errores.append({'archivo: ':nombre_archivo,
                            'url': url,
                        'error: ': 'No se pudo descargar el archivo, verifique el link de descarga'})
            print('error: no se pudo descargar '+nombre_archivo)

            pass

    except(requests.exceptions.ConnectionError):
        errores.append({'archivo: ':nombre_archivo,
                        'url': url,
                        'error: ':'No se pudo descargar el archivo, verifique el link de descarga'})
        print('error: no se pudo descargar '+nombre_archivo+ ', verifique el link de descarga')



#función que define que separación utiliza un archivo '.csv'
def elegir_decimal(nombre_archivo,encoding):
    """
    Abre el archivo indicado y cuenta los puntos y los puntos y comas, devuelve
    el signo más abundante en el archivo como un string.

    Parameters:
    nombre_archivo: El nombre del archivo a analizar.
    encoding: El encoding del archivo, necesario para abrirlo correctamente

    """
    with  open('descargas_calificador/'+nombre_archivo, 'r', encoding=encoding) as f:
        n_punto_y_coma = sum(line.count(';') for line in f)
        f.seek(0)
        n_comas = sum(line.count(',') for line in f)
        if n_comas > n_punto_y_coma:
            return ','
        elif n_comas < n_punto_y_coma:
            return ';'
        else:
            return ','

#función que convierte los archivos '.csv', '.xls' y 'xlsx' en DataFrames
def leer_tabla(nombre_archivo):
    """
    Recibe un archivo '.csv', '.xls' o 'xlsx'  e intenta convertirlo a DataFrame
    probando distintas combinaciones de encodings y separaciones, en el caso de
    los '.csv'. Devuelve un DataFrame si la operación fue exitosa o una variable
    con un string vacío si no pudo procesar el archivo.

    Parameters:
    nombre_archivo: Es el nombre del archivo a analizar

    """
    print('comenzando el procesamiento de archivo')
    encoding_list = ['utf-8', 'utf-16', 'latin', 'ascii', 'ISO-8859-5', 'ISO-8859-1', 'Windows-1252']
    sep_list = [',',';']

    if nombre_archivo.endswith('.xls') or nombre_archivo.endswith('.xlsx'):
        try:
            df = pd.read_excel('./descargas_calificador/'+nombre_archivo, encoding=detect_encoding(nombre_archivo))
            print('archivo procesado')
            return df
        except:
            for enc in encoding_list:
                try:
                    df = pd.read_excel('./descargas_calificador/'+nombre_archivo, encoding=enc)
                    print('archivo procesado')
                    return df
                    break
                except:
                    continue


    elif nombre_archivo.endswith('.csv'):
        auto_encoding = detect_encoding(nombre_archivo)
        df = ''
        for separador in sep_list:
            try:
                df = pd.read_csv('./descargas_calificador/'+nombre_archivo, encoding=auto_encoding, sep=separador)
                decimal = elegir_decimal(nombre_archivo, encoding=auto_encoding)
                df = pd.read_csv('./descargas_calificador/'+nombre_archivo, encoding=auto_encoding , sep=decimal)
                print('archivo procesado')
                return df
                break
            except:
                continue
        if not isinstance(df, pd.DataFrame):
            for enc in encoding_list:
                for separador in sep_list:
                    try:
                        df = pd.read_csv('./descargas_calificador/'+nombre_archivo, encoding=enc , sep=separador)
                        decimal = elegir_decimal(nombre_archivo,encoding=enc)
                        df = pd.read_csv('./descargas_calificador/'+nombre_archivo, encoding=enc , sep=decimal)
                        print('archivo procesado')
                        return df
                        break
                    except:
                        continue
        if not isinstance(df, pd.DataFrame):
            print ('error: no se pudo procesar el archivo, tiene un formato incorrecto')
    else:
        print('error: no se pudo procesar el archivo,  tiene un formato incorrecto')


#función que crea la lista de archivos a analizar y los procesa con leer_tabla()
def correr_analisis(lista, estandar_elegido):
    """
    Analiza la lista de archivos pasada como argumento contrastándola con el estandar elegido.
    Devuelve dos listas con diccionarios llamadas resultados y un csv.

    Parameters
    lista: Lista de links a relevar.
    estandar_elegido: Es el nombre del estandar a relevar.

    """
    #Listas para los resultados del análisis
    resultados = []
    lista_dict = []

    #Busca los archivos que estén marcados como CSV, XLS o XLSX, si no hay sigue con la siguiente hoja
    for index, row in lista.iterrows():
        if not ('CSV' in row['tipo de formato'] or 'XLS' in row['tipo de formato']
                or 'XLSX' in row['tipo de formato']):
            continue

        try:
            r = requests.get(row['¿donde encontraste los datos?'], allow_redirects=True).url
        except(requests.exceptions.ConnectionError):
            continue

        #asigna el nombre al archivo a partir de la última parte del URL
        if r.split('/')[-1] != '':
            nombre_archivo_provisorio = r.split('/')[-1]
        else:
            nombre_archivo_provisorio = r.split('/')[-2]


        #baja el archivo y lo nombra como la última parte de la URL
        bajar_archivo(r, nombre_archivo_provisorio, row['jurisdiccion'])

        #chequeamos falta de extensión de archivo descargado
        if not '.' in nombre_archivo_provisorio:
            extension = magic.from_file('./descargas_calificador/'+nombre_archivo_provisorio,mime=True).split('/')[-1]
            nombre_archivo_nuevo = nombre_archivo_provisorio+'.'+extension
            os.rename('./descargas_calificador/'+nombre_archivo_provisorio, './descargas_calificador/'+nombre_archivo_nuevo)
            nombre_archivo = nombre_archivo_nuevo
        else:
            nombre_archivo = nombre_archivo_provisorio


        #Corrobora que la descarga sea un archivo
        if os.path.isfile('./descargas_calificador/'+nombre_archivo):
            #si es un archivo comprimido extraerá el arhivo que se encuentra dentro
            if nombre_archivo.endswith('.rar') or nombre_archivo.endswith('.tar') or nombre_archivo.endswith('.zip'):
                pl = patoolib.extract_archive('./descargas_calificador/'+nombre_archivo,
                                              outdir='./descargas_calificador/'+nombre_archivo.split('.')[0])

                #si es un archivo válido lo convertirá a Dataframe
                if os.path.isfile(pl):
                    df = leer_tabla(nombre_archivo.split('.')[0] + '/' + nombre_archivo)
                    if not isinstance(df, pd.DataFrame):
                        errores.append({'archivo: ':nombre_archivo.split('.')[0] + '/' + nombre_archivo,
                                        'url': r,
                                        'error: ': 'No se pudo procesar el archivo,  tiene un formato incorrecto'})
                        print('error: no se pudo procesar el archivo,  tiene un formato incorrecto')
                        continue

                #si la descarga es un directorio, buscará dentro de este el documento CSV, XLS o XLSX
                elif os.path.isdir(pl):
                    carpeta = os.listdir('./descargas_calificador/'+nombre_archivo.split('.')[0])
                    files = [f for f in carpeta if f.endswith('.csv') or f.endswith('.xls') or f.endswith('.xlsx')]
                    if files:
                        file = files[0]
                        print(nombre_archivo.split('.')[0]+'/'+file)
                        df = leer_tabla(nombre_archivo.split('.')[0]+'/'+file)
                    else:
                        continue
                    if not isinstance(df, pd.DataFrame):
                        errores.append({'archivo: ':nombre_archivo,
                                        'url': r,
                                        'error: ': 'No se pudo procesar el archivo,  tiene un formato incorrecto'})
                        print('error: no se pudo procesar el archivo,  tiene un formato incorrecto')
                        continue

            elif nombre_archivo.endswith('.xls') or nombre_archivo.endswith('.xlsx') or nombre_archivo.endswith('.csv'):
                df = leer_tabla(nombre_archivo)
                if not isinstance(df, pd.DataFrame):
                    errores.append({'archivo: ':nombre_archivo,
                                    'url': r,
                                    'error: ':'No se pudo procesar el archivo,  tiene un formato incorrecto'})
                    print('error: no se pudo procesar el archivo,  tiene un formato incorrecto')
                    continue

            else:
                errores.append({'archivo:':nombre_archivo,
                                'url': r,
                                'error: ':'No se pudo procesar el archivo,  tiene un formato incorrecto'})
                print('error: no se pudo procesar el archivo,  tiene un formato incorrecto')

            #Toma el Dataframe (df) creado y lo contrasta con el estandar elegido
            if not isinstance(df, pd.DataFrame):
                continue
            partial = calificador(df, estandar_elegido)

            #Si no encuentra el estandar imprime el siguiente mensaje y actualiza el log de errores
            if partial == {'error': 'No se encuentra el estandar'}:
                errores.append({'archivo:':nombre_archivo,
                                'url': r,
                                'error: ':'No se encuentra el estandar correspondiente en taxonomias_de_estandares.csv'})
                print('error: No se encuentra el estandar correspondiente en taxonomias_de_estandares.csv')
                continue
            else:
                evaluation_raw = partial

                #creamos diccionario para CSV
                dict = {
                        'jurisdiccion': row.jurisdiccion,
                        'url': r,
                        'estandar': estandar_elegido,
                        'porcentaje_columnas_aprobadas': partial['estadisticas']['columnas aprobadas']['porcentaje'],
                        'porcentaje_columnas_faltantes': partial['estadisticas']['faltantes del estandar']['porcentaje'],
                        'porcentaje_columnas_fuera_de_estandar': partial['estadisticas']['faltantes del estandar']['porcentaje'],
                        'porcentaje_columnas_casi_aprobadas': partial['estadisticas']['columnas casi aprobadas']['porcentaje'],
                        'columnas_aprobadas': partial['estadisticas']['columnas aprobadas']['absoluto'],
                        'columnas_casi_aprobadas': partial['estadisticas']['columnas casi aprobadas']['absoluto'],
                        'columnas_faltantes': partial['estadisticas']['faltantes del estandar']['absoluto'],
                        'columnas_fuera_de_estandar':partial['estadisticas']['faltantes del estandar']['absoluto'],
                        'dtypes_correctos': partial['estadisticas']['dtypes correctos']
                       }
                lista_dict.append(dict)



                #Creamos resultados_completos
                col = {
                        'jurisdiccion': row.jurisdiccion,
                        'nombre_archivo': nombre_archivo,
                        'estandar': estandar_elegido,
                        'resultados': evaluation_raw

                        }

                resultados.append(col)





        else:
            continue



    if resultados:
        return {'resultados': resultados,
                'lista_dict':lista_dict
               }
    else:
        return 'empty'



calificador_terminal()
